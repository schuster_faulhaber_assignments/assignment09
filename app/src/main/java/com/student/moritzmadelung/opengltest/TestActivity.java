package com.student.moritzmadelung.opengltest;

import android.app.Activity;
import android.opengl.EGLConfig;
import android.opengl.GLSurfaceView;
import android.os.Bundle;
import android.view.Surface;
import android.view.SurfaceView;

import java.util.concurrent.CountDownLatch;

import javax.microedition.khronos.opengles.GL10;

/**
 * Created by localadmin on 29.11.2016.
 */

public class TestActivity extends Activity {

    static CountDownLatch latch;
    static GLSurfaceView surfaceView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        latch = new CountDownLatch(1);
        surfaceView = new GLSurfaceView(this);
        surfaceView.setEGLContextClientVersion(2);
        surfaceView.setRenderer(new EmptyRenderer(latch));
        setContentView(surfaceView);
    }

    public static GLSurfaceView getSurfaceview(){
        if(latch != null){
            try {
                latch.wait();
                return surfaceView;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            latch = null;
        }
        return null;
    }

    public static class EmptyRenderer implements GLSurfaceView.Renderer{
        private CountDownLatch latch;
        public EmptyRenderer(CountDownLatch latch){
            this.latch = latch;
        }

        @Override
        public void onSurfaceCreated(GL10 gl10, javax.microedition.khronos.egl.EGLConfig eglConfig) {

        }

        @Override
        public void onSurfaceChanged(GL10 gl10, int i, int i1) {

        }

        @Override
        public void onDrawFrame(GL10 gl10) {
            if(latch != null){
                latch.countDown();
                latch = null;
            }
        }
    }
}
