package com.student.moritzmadelung.opengltest;

/**
 * Created by localadmin on 29.11.2016.
 */

public abstract class TestWrapper {
    private Error error = null;
    private Throwable throwable = null;

    public void executeWrapper() {
        try {
            executeTest();
        } catch (Error e) {
            synchronized (this) {
                error = e;
            }
        } catch (Throwable t) {
            synchronized (this) {
                throwable = t;
            }
        }
    }

    public void rethrowExceptions() {
        synchronized (this) {
            if (error != null) {
                throw error;
            }
            if (throwable != null) {
                throw new RuntimeException("Unexpected exception", throwable);
            }
        }
    }

    public abstract void executeTest() throws Throwable;
}
