package com.student.moritzmadelung.opengltest;

import android.test.ActivityInstrumentationTestCase2;

import junit.framework.TestCase;

import org.junit.Test;

import static com.student.moritzmadelung.opengltest.TestActivity.latch;

/**
 * Created by localadmin on 29.11.2016.
 */

public class BaseTest extends ActivityInstrumentationTestCase2<TestActivity> {

    public BaseTest() {
        super(TestActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void runOnGLThread(final TestWrapper test) {
        TestActivity.getSurfaceview().queueEvent(new Runnable(){

            @Override
            public void run() {
                test.executeWrapper();
                latch.countDown();
            }
        });
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
